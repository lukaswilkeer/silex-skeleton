Siex-Silicon, the php Silex skeleton app for little php projets.
=====
### The Project.
This is a app skeleton usgin the silex micro-framework.
This skeleton contains.
*   Mustache Template Engine.
*   Silex/Application for core.
*   Symfony/form.
*   Symfony/validator.
*   Symfony web profiler.
*   Various Symfony components!
*   Check the composer.json for more.

### Setting.
First, check the `config/dev.php` and `confi/prod.php`.
On the configuration file, set the `APPDIR` variable.
Don't forget to set write permission to folders `cache/` adn `logs/` folder.

### How to create a project?
Using composer,`composer create-project lukaswilkeer/silex-silicon somewhere/ 0.5.x-dev`,
or clone thi repository.

### Contributing.
Fork this repository, create a branch, submit a pull request with your feature.

### Contact.
Lukas Willkeer | @lukaswilkeer
